<?php

namespace Blueberry\Utility;

class Request
{
    private static $routeMappings = [];
    public static function on($uri, $action, $method = 'GET')
    {
        // Do mapping
    }
    public static function onGet($uri, $action)
    {
        self::on($uri, $action, 'GET');
    }
}