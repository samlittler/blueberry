<?php

use Blueberry\Blueberry;

session_start(); // TODO: Move this somewhere else

require '../vendor/autoload.php';
$blueberry = new Blueberry();
$blueberry->run();